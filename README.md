# Team A App1

This project contains a very simple app that can be used to demo the functionality of ArgoCD.

## Usage

Run the following to create a new version of the app.  The script will update the version and then do a git add, commit, tag and push.  The new tag is seen by ArgoCD

```sh
./mkrelease.sh 
```


#!/bin/bash
# set -x			# activate debugging from here
# set +x			# stop debugging from here

CURRENT_VERSION=$(cat VERSION)
NEW_VERSION=$(($CURRENT_VERSION + 1))
UUID_UPPER=$(uuidgen)
UUID=$(echo $UUID_UPPER | tr '[:upper:]' '[:lower:]')
IMAGE="ttl.sh/$UUID:4h" # nees to be all lower case

printf "$NEW_VERSION" > VERSION
echo "Make new release v$NEW_VERSION"

#
#  Set version number in app content
#
echo "<!DOCTYPE html>
<html>
<head>
<style>
body {text-align: center;}
</style>
<title>Team-A App</title>
</head>
<body>
  <h1>Team-A</h1>
  <h2>App1 Version: v$NEW_VERSION</h2>
</body>
</html>
" > index.html

#
# Update deployment manifest
#
echo "Using $IMAGE"
img="$IMAGE" yq '.spec.template.spec.containers[0].image = strenv(img)' k8s/deployment.yaml --inplace
ver="$NEW_VERSION" yq '.metadata.labels.app-ver = "v"+strenv(ver)' k8s/deployment.yaml --inplace
# img="$IMAGE" yq '.variables.IMAGE = strenv(img)' .gitlab-ci.yml --inplace

#
# Build and push image
#
podman build -t $IMAGE .
podman push $IMAGE
#
# Commit new release and tag it for argo
#
git add VERSION index.html k8s/deployment.yaml
git commit -m "make release $NEW_VERSION"
git tag v$NEW_VERSION -m "Version v$NEW_VERSION released"
git push --tags
git push